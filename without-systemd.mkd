Title: Without systemd
CSS: /css/main.css

#{include head}

# Without systemd

## Overview

For users that may desire a system without any systemd dependencies, this
document aims to explain how it can be done, and possible caveats of using
Exherbo in the post-systemd-adoption world.

[pkg:sys-apps/systemd] will remain the default init system for the forseeable
future, because it is the one most users would expect nowadays, and is probably
the most battle tested, in terms of adoption in Linux distributons. With that
said, it is certainly replaceable and can be done fairly easily on Exherbo.

## Process

For glibc targets, the `[systemd]` option is enabled by default, and as such
will need to be disabled to remove any library-related dependencies on systemd.

In addition, you should disable any systemd `[providers:*]` options.

    */* providers: -systemd
    */* -systemd

On non-glibc targets, this isn't applicable and no option changes are needed.

Since a udev implementation is required by a few system packages (and really
most packages that deal with hardware), you'll have to use an alternative
implementation of udev; currently the only alternative is [pkg:sys-apps/eudev].

Since systemd also provides some other services, such as `logind`, `journald`,
etc. you will need to find alternatives.

[pkg:sys-auth/ConsoleKit2] works fine for a logind replacement, and you can use
any one of the [pkg:virtual/syslog] providers for replacing `journald`.

    */* providers: -systemd eudev rsyslog
    */* -systemd consolekit

Lastly, and probably most importantly, you'll need to find a new init daemon and
service manager.

## Replacing init

### Full-featured replacements

These are programs which provide an init daemon (PID 1), and a service manager.

- [pkg:sys-apps/runit]; TODO: May need some documentation added for using it. Ask woutershep...

### Init daemons

- [pkg:sys-apps/sinit]
- [pkg:sys-apps/sysvinit]
- [pkg:sys-apps/s6-linux-init]

### Service managers

- [pkg:sys-apps/s6]
- [pkg:sys-apps/openrc]

There's countless more implementations available, but they're probably not
packaged. That's a hint to you to package them if you want to use them. :)

## Notes

[Systemd](systemd.html) is the official init and service manager of
[Exherbo](http://exherbo.org), thus it has the most support.
If you are using another service manager, rc services might be lacking for some packages.


--
Copyright 2015 Kylie McClain

#{include CC_3.0_Attribution}
#{include foot}

<!-- vim: set tw=100 ft=mkd spell spelllang=en sw=4 sts=4 et : -->
